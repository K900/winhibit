use anyhow::Result;
use widestring::U16CString;
use windows::core::PWSTR;
use windows::Win32::{
    Foundation::{CloseHandle, HANDLE},
    System::{
        Power::{
            PowerCreateRequest, PowerRequestDisplayRequired, PowerRequestSystemRequired,
            PowerSetRequest,
        },
        SystemServices::POWER_REQUEST_CONTEXT_VERSION,
        Threading::{POWER_REQUEST_CONTEXT_SIMPLE_STRING, REASON_CONTEXT, REASON_CONTEXT_0},
    },
};

pub struct WakeLock {
    pub(crate) handle: HANDLE,
    pub(crate) reason: PWSTR,
}

impl WakeLock {
    pub fn new(reason: String, keep_display: bool) -> Result<Self> {
        let reason = PWSTR(U16CString::from_str(reason)?.into_raw());

        let context = REASON_CONTEXT {
            Version: POWER_REQUEST_CONTEXT_VERSION,
            Flags: POWER_REQUEST_CONTEXT_SIMPLE_STRING,
            Reason: REASON_CONTEXT_0 {
                SimpleReasonString: reason,
            },
        };

        let handle = unsafe { PowerCreateRequest(&context) }?;

        unsafe {
            PowerSetRequest(handle, PowerRequestSystemRequired)?;
        }

        if keep_display {
            unsafe {
                PowerSetRequest(handle, PowerRequestDisplayRequired)?;
            }
        }

        Ok(Self { handle, reason })
    }
}

impl Drop for WakeLock {
    fn drop(&mut self) {
        unsafe {
            if !self.reason.is_null() {
                let _ = U16CString::from_raw(self.reason.0);
            }
            let _ = CloseHandle(self.handle);
        }
    }
}

unsafe impl Send for WakeLock {}
