use std::{process::Command, thread::sleep, time::Duration};

use anyhow::{bail, Context, Result};
use clap::{ArgGroup, Parser};
use wakelock::WakeLock;

mod wakelock;

/// Inhibit sleep while a command is running, kind of like a bad systemd-inhibit.
#[derive(Parser)]
#[clap(author = "K900 <me@0upti.me>", group = ArgGroup::new("commands").required(true))]
struct Opts {
    #[clap(short, long)]
    /// Reason to provide to Windows. Defaults to the command being ran.
    reason: Option<String>,

    #[clap(group = "commands")]
    /// Command to run.
    command: Vec<String>,

    #[clap(short, long)]
    /// Keep display on.
    keep_display: bool,

    #[clap(short, long, group = "commands")]
    /// Instead of running a command, sleep until killed.
    zzzz: bool,
}

fn main() -> Result<()> {
    let opts: Opts = Opts::parse();

    let _guard = WakeLock::new(
        opts.reason.unwrap_or_else(|| {
            if !opts.command.is_empty() {
                opts.command.join(" ")
            } else {
                "sleeping...".into()
            }
        }),
        opts.keep_display,
    )
    .context("Failed to acquire wake lock")?;

    if opts.zzzz {
        loop {
            sleep(Duration::from_secs(24 * 60 * 60))
        }
    } else {
        let exit = Command::new(&opts.command[0])
            .args(&opts.command[1..])
            .status()
            .context("Failed to start process")?;

        if exit.success() {
            Ok(())
        } else {
            bail!("Process failed: {}", exit);
        }
    }
}
