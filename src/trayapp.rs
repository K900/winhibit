#![windows_subsystem = "windows"]

use anyhow::Result;
use tao::{
    event::Event,
    event_loop::{ControlFlow, EventLoopBuilder},
    platform::run_return::EventLoopExtRunReturn,
};
use tray_icon::{
    menu::{CheckMenuItem, Menu, MenuEvent, MenuItem, PredefinedMenuItem},
    Icon, MouseButton, MouseButtonState, TrayIconBuilder, TrayIconEvent,
};
use wakelock::WakeLock;

mod wakelock;

const UNLOCKED: &str = "Not keeping awake";
const LOCKED: &str = "Keeping awake";

const UNLOCKED_ICON: &[u8] = include_bytes!("icons/unlocked.png");
const LOCKED_ICON: &[u8] = include_bytes!("icons/locked.png");

fn load_icon(buf: &[u8]) -> Icon {
    let (icon_rgba, icon_width, icon_height) = {
        let image = image::load_from_memory(buf)
            .expect("Failed to open icon path")
            .into_rgba8();
        let (width, height) = image.dimensions();
        let rgba = image.into_raw();
        (rgba, width, height)
    };
    Icon::from_rgba(icon_rgba, icon_width, icon_height).expect("Failed to open icon")
}

struct Locker {
    lock: Option<WakeLock>,
    keep_display: bool,
}

impl Locker {
    fn new() -> Self {
        Self {
            lock: None,
            keep_display: true,
        }
    }

    fn lock(&mut self) -> anyhow::Result<()> {
        self.lock = Some(WakeLock::new(
            "Inhibited by tray icon".into(),
            self.keep_display,
        )?);
        Ok(())
    }

    fn unlock(&mut self) {
        self.lock = None
    }

    fn is_locked(&self) -> bool {
        self.lock.is_some()
    }

    fn toggle(&mut self) -> anyhow::Result<()> {
        if self.is_locked() {
            self.unlock();
        } else {
            self.lock()?;
        }

        Ok(())
    }
}

pub enum EventLoopMessage {
    MenuEvent(MenuEvent),
    TrayIconEvent(TrayIconEvent),
}

fn main() -> Result<()> {
    let tray_menu = Menu::new();

    let keep_awake = CheckMenuItem::new("Keep awake", true, false, None);
    tray_menu.append(&keep_awake)?;
    let keep_display = CheckMenuItem::new("Keep display", true, true, None);
    tray_menu.append(&keep_display)?;

    tray_menu.append(&PredefinedMenuItem::separator())?;

    let quit = MenuItem::new("Quit", true, None);
    tray_menu.append(&quit)?;

    let mut event_loop = EventLoopBuilder::with_user_event().build();

    let unlocked_icon = load_icon(UNLOCKED_ICON);
    let locked_icon: Icon = load_icon(LOCKED_ICON);

    let tray = TrayIconBuilder::new()
        .with_icon(unlocked_icon.clone())
        .with_menu(Box::new(tray_menu))
        .with_tooltip(UNLOCKED)
        .build()?;

    let mut locker = Locker::new();

    let proxy = event_loop.create_proxy();
    MenuEvent::set_event_handler(Some(move |e: MenuEvent| {
        let _ = proxy.send_event(EventLoopMessage::MenuEvent(e));
    }));

    let proxy = event_loop.create_proxy();
    TrayIconEvent::set_event_handler(Some(move |e: tray_icon::TrayIconEvent| {
        let _ = proxy.send_event(EventLoopMessage::TrayIconEvent(e));
    }));

    event_loop.run_return(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;
        let mut need_update = false;

        let mut try_toggle = || {
            if let Err(e) = locker.toggle() {
                println!("Failed to take lock: {e:?}");
            };
            need_update = true;
        };

        match event {
            Event::UserEvent(EventLoopMessage::MenuEvent(MenuEvent { id, .. })) => {
                if id == quit.id() {
                    *control_flow = ControlFlow::Exit;
                }

                if id == keep_awake.id() {
                    try_toggle();
                }

                if id == keep_display.id() {
                    locker.keep_display = !locker.keep_display;
                    keep_display.set_checked(locker.keep_display);
                    if locker.is_locked() {
                        locker.unlock();
                        if let Err(e) = locker.lock() {
                            println!("Failed to take lock: {e:?}");
                        };
                    }
                }
            }
            Event::UserEvent(EventLoopMessage::TrayIconEvent(TrayIconEvent::Click {
                button: MouseButton::Left,
                button_state: MouseButtonState::Down,
                ..
            })) => {
                try_toggle();
            }
            _ => {}
        }

        if need_update {
            let locked = locker.is_locked();
            keep_awake.set_checked(locked);

            if locked {
                let _ = tray.set_icon(Some(locked_icon.clone()));
                let _ = tray.set_tooltip(Some(LOCKED));
            } else {
                let _ = tray.set_icon(Some(unlocked_icon.clone()));
                let _ = tray.set_tooltip(Some(UNLOCKED));
            }
        }
    });

    Ok(())
}
