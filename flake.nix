{
  description = "Here be heinous crimes";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
    cross = {
      url = "gitlab:K900/rust-cross-windows";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        pre-commit-hooks.follows = "pre-commit-hooks";
      };
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    {
      self,
      nixpkgs,
      cross,
      pre-commit-hooks,
      ...
    }:
    let
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
    in
    {
      packages.x86_64-linux = rec {
        winhibitWindows = cross.lib.mkCrossPackage {
          src = ./.;
        };

        winhibit = pkgs.writeShellScriptBin "winhibit" ''
          ${winhibitWindows}/bin/winhibit.exe --zzzz --keep-display --reason="[WSL] $*" &
          pid=$!

          trap "(kill -s 0 $pid && kill -9 $pid) &> /dev/null" EXIT

          "$@"
        '';

        default = winhibit;
      };

      checks.x86_64-linux.pre-commit-check = pre-commit-hooks.lib.x86_64-linux.run {
        src = ./.;

        tools = {
          cargo = cross.lib.crossToolchain;
          clippy = cross.lib.crossToolchain;
        };

        hooks = {
          nixfmt-rfc-style.enable = true;
          deadnix.enable = true;
          statix.enable = true;

          rustfmt.enable = true;
          clippy.enable = true;
          cargo-check.enable = true;
        };
      };

      devShells.x86_64-linux.default = cross.lib.mkCrossShell {
        inputsFrom = [ self.packages.x86_64-linux.winhibitWindows ];
        buildInputs = [
          pkgs.cargo-outdated
          pkgs.rustfmt
        ];

        inherit (self.checks.x86_64-linux.pre-commit-check) shellHook;
      };
    };
}
